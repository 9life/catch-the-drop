import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class GameWindow extends JFrame {
    private static GameWindow game_window;
    private static long last_frame_time;
    private static Image background;
    private static Image drop;
    private static Image game_over;
    private static float drop_left = 200; //координата х левого верхнего угла экрана
    private static float drop_top = -100;//координата y левого верхнего угла экрана
    private static float drop_v = 200; //скорость капли
    private static int score;

    public static void main(String[] args) throws IOException {
        background = ImageIO.read(GameWindow.class.getResourceAsStream("background.png"));
        drop = ImageIO.read(GameWindow.class.getResourceAsStream("drop.png"));
        game_over = ImageIO.read(GameWindow.class.getResourceAsStream("game_over.png"));
        game_window = new GameWindow();
        game_window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); //при закрытии окна программа будет
        //завершаться
        game_window.setLocation(200, 100); //точка, в которой будет появляться окно
        game_window.setSize(906, 480); //размеры окна
        game_window.setResizable(false); //изменение размеров можно или нельзя
        last_frame_time = System.nanoTime(); //текущее время в нано секундах
        GameField game_field = new GameField();
        game_field.addMouseListener(new MouseAdapter() { //что-то будет происходить после нажатия кнопки мыши
            @Override
            public void mousePressed(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                float drop_right = drop_left + drop.getWidth(null); //верхняя граница точки
                float drop_bottom = drop_top + drop.getHeight(null); //нижняя граница точки
                boolean is_drop = x >= drop_left && x <= drop_right && y >= drop_top && y <= drop_bottom;
                if (is_drop) {
                    drop_top = -100;
                    drop_left = (int) (Math.random() * (game_field.getWidth() - drop.getWidth(null)));
                    drop_v = drop_v + 20;
                    score++; //считаем очки
                    game_window.setTitle("Score: " + score); //выводим в названии окна очки
                }
            }
        });
        game_window.add(game_field);
        game_window.setVisible(true); //по умолчанию окно скрыто
    }

    private static void onRepaint(Graphics g) {
        long current_time = System.nanoTime();
        float delta_time = (current_time - last_frame_time) * 0.000000001f;
        last_frame_time = current_time;
        drop_top = drop_top + drop_v * delta_time;
        g.drawImage(background, 0, 0, null);
        g.drawImage(drop, (int) drop_left, (int) drop_top, null);
        if (drop_top > game_window.getHeight()) //если капля улетает за пределы окна, то игра гейм овер
            g.drawImage(game_over, 280, 120, null);
    }

    private static class GameField extends JPanel {
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g); //отрисовывается панель
            onRepaint(g);
            repaint();
        }
    }
}
